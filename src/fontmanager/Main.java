/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fontmanager;
import java.util.prefs.BackingStoreException;

/**
 *
 * @author M.Link
 */
public class Main {
  /** Main method to this application.
   * 
   * @params args   Argument delivered to this application.
   */
  public static void main(String[] args) throws BackingStoreException {    
    (new Viewer()).setVisible(true);
  }
}
