package fontmanager;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author  root
 */
public class Viewer extends JFrame {
  protected static final String EXAMPLE_TEXT_EN 
    = "The quick brown fox jumps over the lazy dog.";
  protected static final String EXAMPLE_TEXT_DE 
    = "Zwölf Boxkämpfer jagen Viktor quer über den großen Sylter Deich.";
  protected static final String EXAMPLE_TEXT_NUMBERS
    = "0123456789";
  protected static final String REGISTRY_FONTS_INSTALLED 
    = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"
      + "\\Fonts";
  
  protected StyledDocument previewDocument;
  protected int previewStyle;
  protected String systemFontPath;
  protected WinRegAccess registry;
  protected TreeSet<FontFile> installedFonts;
  protected TreeSet<FontFile> loadedFonts;
  
  protected class ColoredFontFileListCellRender 
      extends DefaultListCellRenderer {
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value,
        int index, boolean isSelected, boolean cellHasFocus) {

      super.getListCellRendererComponent(list, value, index, isSelected, 
        cellHasFocus);
        
      if (value instanceof FontFile) {
        FontFile ff = (FontFile) value;
                
        if (!ff.getFile().getPath().equals(System.getenv("windir") + "\\Fonts")) {
          this.setBackground(Color.LIGHT_GRAY);
        }
      }
      
      return this;
    }
  }
  
  /** Creates new form Viewer */
  public Viewer() {
    this.previewDocument = new DefaultStyledDocument();
    this.registry = new WinRegAccess();
    this.installedFonts = new TreeSet<FontFile>();
    this.loadedFonts = new TreeSet<FontFile>();
    
    this.previewStyle = Font.PLAIN;
    
    try { 
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 
    } catch(Exception e) {
      // Do nothing, because the L&F is not that important.
    }
    
    this.setExtendedState(JFrame.MAXIMIZED_BOTH);
    
    initComponents();
    
    this.listFonts.setCellRenderer(new ColoredFontFileListCellRender());
    
    this.systemFontPath = System.getenv("windir") + "\\Fonts";
    
    this.comboBoxPreviewSize.setSelectedIndex(6);

    this.textPanePreview.setStyledDocument(this.previewDocument);
    this.textPanePreview.setText(Viewer.EXAMPLE_TEXT_NUMBERS + "\n" 
      + Viewer.EXAMPLE_TEXT_EN + "\n" + Viewer.EXAMPLE_TEXT_DE);

    this.buttonSelectPath.setEnabled(false);
    this.loadFontsInstalled();
    this.showFontsInstalled();
    
    this.comboBoxPreviewSizeItemStateChanged(null);
  }
  
  protected boolean haveAllFontsBeenLoaded() {
    return ((this.listFonts.getModel().getSize() > 0) 
      && (this.listFonts.getModel().getSize() > 0));
  }
  
  protected void showFontsInstalled() {
    this.listFonts.setListData(this.installedFonts.toArray());
  }
  
  protected void loadFontsInstalled() {
    TreeMap<String, ArrayList<String>> registeredFonts 
      = this.registry.getNodeChildren(Viewer.REGISTRY_FONTS_INSTALLED, 
        WinRegAccess.STRING);
    this.installedFonts.clear();
    
    if (!registeredFonts.isEmpty()) {
      Set<String> keySet = registeredFonts.keySet();
      Iterator<String> keyIterator = keySet.iterator();

      while(keyIterator.hasNext()) {
        String fontName = keyIterator.next();
        ArrayList<String> keyItems = registeredFonts.get(fontName);
        if (keyItems.size() > 1) {
          String fontFileName = keyItems.get(1);
        
          if (fontFileName.toLowerCase().endsWith(".ttf")) {
            if (!fontFileName.startsWith("\\\\") 
                && (fontFileName.charAt(1) != ':')) {
              fontFileName = this.systemFontPath + "\\" + fontFileName;
            }

            File file = new File(fontFileName);

            FontFile fontFile = new FontFile(file);

            try {
              fontFile.load();
              
              this.installedFonts.add(fontFile);
            } catch (Exception e) {
              System.out.println(e.getMessage());
            }
          }
        }
      }
    }
  }
  
  protected void loadFontsFromDir(String dir) {
    File folder = new File(dir);
    File[] files = folder.listFiles();
    
    this.loadedFonts.clear();
    
    if (files != null) {
      for (int i = 0; i < files.length; i++) {
        if (files[i].getName().toLowerCase().endsWith(".ttf")) {
          FontFile fontFile = new FontFile(files[i]);
          
          try {
            fontFile.load();
            
            this.loadedFonts.add(fontFile);
          } catch (Exception e) {
            System.out.println(e.getMessage());
          }
        }
      }
    }
  }
  
  protected void setPreviewFont(Font font) {
    this.textPanePreview.setFont(font.deriveFont(this.previewStyle, 
      Float.valueOf((String) 
        this.comboBoxPreviewSize.getSelectedItem()).floatValue()));
    this.previewDocument.setCharacterAttributes(0, 
      this.textPanePreview.getText().length() - 1, new SimpleAttributeSet(), 
        false);
  }
  
  protected void setPath(String path) {
    this.textFieldSelectedPath.setText(path);
    this.loadFontsFromDir(path);
  }
  
  /** This method is called from within the constructor to
   * initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is
   * always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    buttonGroup1 = new javax.swing.ButtonGroup();
    splitPaneDirView = new javax.swing.JSplitPane();
    panelFontList = new javax.swing.JPanel();
    panelSource = new javax.swing.JPanel();
    panelInstalled = new javax.swing.JPanel();
    radioButtonInstalled = new javax.swing.JRadioButton();
    panelDirectory = new javax.swing.JPanel();
    radioButtonDirectory = new javax.swing.JRadioButton();
    panelSelectPath = new javax.swing.JPanel();
    labelSelectedPath = new javax.swing.JLabel();
    buttonSelectPath = new javax.swing.JButton();
    textFieldSelectedPath = new javax.swing.JTextField();
    panelList = new javax.swing.JPanel();
    labelFontList = new javax.swing.JLabel();
    panelFontCounter = new javax.swing.JPanel();
    labelFontsLoaded = new javax.swing.JLabel();
    labelFontsCount = new javax.swing.JLabel();
    scrollPaneFontList = new javax.swing.JScrollPane();
    listFonts = new javax.swing.JList();
    panelView = new javax.swing.JPanel();
    toolBarPreviewSettings = new javax.swing.JToolBar();
    labelPreviewSize = new javax.swing.JLabel();
    comboBoxPreviewSize = new javax.swing.JComboBox();
    jSeparator1 = new javax.swing.JToolBar.Separator();
    labelPreviewStyle = new javax.swing.JLabel();
    toggleButtonBold = new javax.swing.JToggleButton();
    toggleButtonItalic = new javax.swing.JToggleButton();
    scrollPanePreview = new javax.swing.JScrollPane();
    textPanePreview = new javax.swing.JTextPane();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("fontmanager/Bundle"); // NOI18N
    setTitle(bundle.getString("Viewer.title")); // NOI18N
    setMinimumSize(new java.awt.Dimension(600, 400));

    splitPaneDirView.setDividerLocation(350);

    panelFontList.setLayout(new java.awt.BorderLayout());

    panelSource.setBorder(javax.swing.BorderFactory.createTitledBorder(bundle.getString("Viewer.panelSource.border.title"))); // NOI18N
    panelSource.setLayout(new java.awt.BorderLayout());

    panelInstalled.setLayout(new java.awt.BorderLayout());

    buttonGroup1.add(radioButtonInstalled);
    radioButtonInstalled.setSelected(true);
    radioButtonInstalled.setText(bundle.getString("Viewer.radioButtonInstalled.text")); // NOI18N
    radioButtonInstalled.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
        radioButtonInstalledItemStateChanged(evt);
      }
    });
    panelInstalled.add(radioButtonInstalled, java.awt.BorderLayout.NORTH);

    panelSource.add(panelInstalled, java.awt.BorderLayout.PAGE_START);

    panelDirectory.setLayout(new java.awt.BorderLayout());

    buttonGroup1.add(radioButtonDirectory);
    radioButtonDirectory.setText(bundle.getString("Viewer.radioButtonDirectory.text")); // NOI18N
    radioButtonDirectory.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
        radioButtonDirectoryItemStateChanged(evt);
      }
    });
    panelDirectory.add(radioButtonDirectory, java.awt.BorderLayout.NORTH);

    panelSelectPath.setLayout(new java.awt.BorderLayout());

    labelSelectedPath.setText(bundle.getString("Viewer.labelSelectedPath.text")); // NOI18N
    panelSelectPath.add(labelSelectedPath, java.awt.BorderLayout.NORTH);

    buttonSelectPath.setText(bundle.getString("Viewer.buttonSelectPath.text")); // NOI18N
    buttonSelectPath.setToolTipText(bundle.getString("Viewer.buttonSelectPath.toolTipText")); // NOI18N
    buttonSelectPath.setEnabled(false);
    buttonSelectPath.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        buttonSelectPathActionPerformed(evt);
      }
    });
    panelSelectPath.add(buttonSelectPath, java.awt.BorderLayout.EAST);

    textFieldSelectedPath.setText(bundle.getString("Viewer.textFieldSelectedPath.text")); // NOI18N
    textFieldSelectedPath.setEnabled(false);
    panelSelectPath.add(textFieldSelectedPath, java.awt.BorderLayout.CENTER);

    panelDirectory.add(panelSelectPath, java.awt.BorderLayout.CENTER);

    panelSource.add(panelDirectory, java.awt.BorderLayout.CENTER);

    panelFontList.add(panelSource, java.awt.BorderLayout.NORTH);

    panelList.setLayout(new java.awt.BorderLayout());

    labelFontList.setText(bundle.getString("Viewer.labelFontList.text")); // NOI18N
    panelList.add(labelFontList, java.awt.BorderLayout.NORTH);

    panelFontCounter.setLayout(new java.awt.BorderLayout());

    labelFontsLoaded.setText(bundle.getString("Viewer.labelFontsLoaded.text")); // NOI18N
    panelFontCounter.add(labelFontsLoaded, java.awt.BorderLayout.WEST);

    labelFontsCount.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
    labelFontsCount.setText(bundle.getString("Viewer.labelFontsCount.text")); // NOI18N
    panelFontCounter.add(labelFontsCount, java.awt.BorderLayout.CENTER);

    panelList.add(panelFontCounter, java.awt.BorderLayout.SOUTH);

    listFonts.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    listFonts.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
      public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
        listFontsValueChanged(evt);
      }
    });
    scrollPaneFontList.setViewportView(listFonts);

    panelList.add(scrollPaneFontList, java.awt.BorderLayout.CENTER);

    panelFontList.add(panelList, java.awt.BorderLayout.CENTER);

    splitPaneDirView.setLeftComponent(panelFontList);

    panelView.setLayout(new java.awt.BorderLayout());

    toolBarPreviewSettings.setFloatable(false);
    toolBarPreviewSettings.setRollover(true);
    toolBarPreviewSettings.setAlignmentY(0.5F);

    labelPreviewSize.setText(bundle.getString("Viewer.labelPreviewSize.text")); // NOI18N
    toolBarPreviewSettings.add(labelPreviewSize);

    comboBoxPreviewSize.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "6", "8", "10", "12", "14", "16", "20", "24", "28", "32", "40", "48", "56", "64", "72", "80", "100" }));
    comboBoxPreviewSize.setMaximumSize(new java.awt.Dimension(50, 18));
    comboBoxPreviewSize.setMinimumSize(new java.awt.Dimension(50, 18));
    comboBoxPreviewSize.setPreferredSize(new java.awt.Dimension(50, 18));
    comboBoxPreviewSize.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
        comboBoxPreviewSizeItemStateChanged(evt);
      }
    });
    toolBarPreviewSettings.add(comboBoxPreviewSize);
    toolBarPreviewSettings.add(jSeparator1);

    labelPreviewStyle.setText(bundle.getString("Viewer.labelPreviewStyle.text")); // NOI18N
    toolBarPreviewSettings.add(labelPreviewStyle);
    labelPreviewStyle.getAccessibleContext().setAccessibleName(bundle.getString("Viewer.labelPreviewStyle.AccessibleContext.accessibleName")); // NOI18N

    toggleButtonBold.setFont(new java.awt.Font("Tahoma", 0, 14));
    toggleButtonBold.setText(bundle.getString("Viewer.toggleButtonBold.text")); // NOI18N
    toggleButtonBold.setToolTipText(bundle.getString("Viewer.toggleButtonBold.toolTipText")); // NOI18N
    toggleButtonBold.setFocusable(false);
    toggleButtonBold.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    toggleButtonBold.setMaximumSize(new java.awt.Dimension(32, 32));
    toggleButtonBold.setMinimumSize(new java.awt.Dimension(32, 32));
    toggleButtonBold.setPreferredSize(new java.awt.Dimension(32, 32));
    toggleButtonBold.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toggleButtonBold.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        toggleButtonBoldActionPerformed(evt);
      }
    });
    toolBarPreviewSettings.add(toggleButtonBold);

    toggleButtonItalic.setFont(new java.awt.Font("Tahoma", 0, 14));
    toggleButtonItalic.setText(bundle.getString("Viewer.toggleButtonItalic.text")); // NOI18N
    toggleButtonItalic.setToolTipText(bundle.getString("Viewer.toggleButtonItalic.toolTipText")); // NOI18N
    toggleButtonItalic.setFocusable(false);
    toggleButtonItalic.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    toggleButtonItalic.setMaximumSize(new java.awt.Dimension(32, 32));
    toggleButtonItalic.setMinimumSize(new java.awt.Dimension(32, 32));
    toggleButtonItalic.setPreferredSize(new java.awt.Dimension(32, 32));
    toggleButtonItalic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toggleButtonItalic.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        toggleButtonItalicActionPerformed(evt);
      }
    });
    toolBarPreviewSettings.add(toggleButtonItalic);

    panelView.add(toolBarPreviewSettings, java.awt.BorderLayout.NORTH);

    scrollPanePreview.setViewportView(textPanePreview);

    panelView.add(scrollPanePreview, java.awt.BorderLayout.CENTER);

    splitPaneDirView.setRightComponent(panelView);

    getContentPane().add(splitPaneDirView, java.awt.BorderLayout.CENTER);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void buttonSelectPathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSelectPathActionPerformed
    JFileChooser pathSelector 
      = new JFileChooser(this.textFieldSelectedPath.getText());
    
    pathSelector.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    pathSelector.setMultiSelectionEnabled(false);
    pathSelector.setDialogType(JFileChooser.OPEN_DIALOG);
    
    if (pathSelector.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      this.setPath(pathSelector.getSelectedFile().toString());
    }
  }//GEN-LAST:event_buttonSelectPathActionPerformed

  private void toggleButtonBoldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleButtonBoldActionPerformed
    if (toggleButtonBold.isSelected()) {
      this.previewStyle |= Font.BOLD;
    } else {
      this.previewStyle &= ~Font.BOLD;
    }
    

  }//GEN-LAST:event_toggleButtonBoldActionPerformed

  private void toggleButtonItalicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toggleButtonItalicActionPerformed
    if (toggleButtonItalic.isSelected()) {
      this.previewStyle |= Font.ITALIC;
    } else {
      this.previewStyle &= ~Font.ITALIC;
    }
    

  }//GEN-LAST:event_toggleButtonItalicActionPerformed

  private void listFontsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listFontsValueChanged
    if (this.listFonts.getModel().getSize() > 0) {
      this.textPanePreview.setFont(
        ((FontFile) this.listFonts.getSelectedValue()).getFont());
    }
}//GEN-LAST:event_listFontsValueChanged

  private void radioButtonInstalledItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioButtonInstalledItemStateChanged
    this.buttonSelectPath.setEnabled(false);
    this.loadFontsInstalled();
    this.showFontsInstalled();
  }//GEN-LAST:event_radioButtonInstalledItemStateChanged

  private void radioButtonDirectoryItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioButtonDirectoryItemStateChanged
    this.buttonSelectPath.setEnabled(true);
  }//GEN-LAST:event_radioButtonDirectoryItemStateChanged

  private void comboBoxPreviewSizeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxPreviewSizeItemStateChanged
    SimpleAttributeSet attributes = new SimpleAttributeSet();
    StyleConstants.setFontSize(attributes, 
      Integer.valueOf((String) this.comboBoxPreviewSize.getSelectedItem()));
    
    this.previewDocument.setCharacterAttributes(0, 
      this.textPanePreview.getText().length() - 1, attributes, false);
  }//GEN-LAST:event_comboBoxPreviewSizeItemStateChanged
  
  // Variables declaration - do not modify//GEN-BEGIN:variables
  protected javax.swing.ButtonGroup buttonGroup1;
  protected javax.swing.JButton buttonSelectPath;
  protected javax.swing.JComboBox comboBoxPreviewSize;
  protected javax.swing.JToolBar.Separator jSeparator1;
  protected javax.swing.JLabel labelFontList;
  protected javax.swing.JLabel labelFontsCount;
  protected javax.swing.JLabel labelFontsLoaded;
  protected javax.swing.JLabel labelPreviewSize;
  protected javax.swing.JLabel labelPreviewStyle;
  protected javax.swing.JLabel labelSelectedPath;
  protected javax.swing.JList listFonts;
  protected javax.swing.JPanel panelDirectory;
  protected javax.swing.JPanel panelFontCounter;
  protected javax.swing.JPanel panelFontList;
  protected javax.swing.JPanel panelInstalled;
  protected javax.swing.JPanel panelList;
  protected javax.swing.JPanel panelSelectPath;
  protected javax.swing.JPanel panelSource;
  protected javax.swing.JPanel panelView;
  protected javax.swing.JRadioButton radioButtonDirectory;
  protected javax.swing.JRadioButton radioButtonInstalled;
  protected javax.swing.JScrollPane scrollPaneFontList;
  protected javax.swing.JScrollPane scrollPanePreview;
  protected javax.swing.JSplitPane splitPaneDirView;
  protected javax.swing.JTextField textFieldSelectedPath;
  protected javax.swing.JTextPane textPanePreview;
  protected javax.swing.JToggleButton toggleButtonBold;
  protected javax.swing.JToggleButton toggleButtonItalic;
  protected javax.swing.JToolBar toolBarPreviewSettings;
  // End of variables declaration//GEN-END:variables

}
