/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fontmanager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author M.Link
 */
public class WinRegAccess {
  protected static final String REG_PROC = "REG";
  protected static final String REG_ADD = "ADD";
  protected static final String REG_DELETE = "DELETE";
  protected static final String REG_QUERY = "QUERY";
  
  public static final String STRING = "REG_SZ";
  public static final String BINARY = "REG_BINARY";
  public static final String DWORD = "REG_DWORD";
  public static final String MULTILINE_STRING = "REG_MULTI_SZ";
  public static final String EXPANDABLE_STRING = "REG_EXPAND_SZ";
  public static final String NODE = "REG_NODE";
    
  public WinRegAccess() {
  }
  
  protected String[] getRegProcResult(String command) {
    String[] result = null;
    
    try {
      Process process = Runtime.getRuntime().exec(command);
      ProcessOutputReader procOutReader = new ProcessOutputReader(process); 
      
      procOutReader.start();
      process.waitFor();
      procOutReader.join();
      
      result = procOutReader.getOutput();
    } catch (Exception e) {
      Logger.getLogger(WinRegAccess.class.getName()).log(Level.SEVERE, null, e);
    }
    
    return result;
  }
  
  public TreeMap<String, ArrayList<String>> getNodeChildren(
      String nodePath) {
    return this.getNodeChildren(nodePath, null);
  }
  
  public TreeMap<String, ArrayList<String>> getNodeChildren(
      String nodePath, String typeFilter) {
    TreeMap<String, ArrayList<String>> result 
      = new TreeMap<String, ArrayList<String>>();
    String command = WinRegAccess.REG_PROC + " " + WinRegAccess.REG_QUERY 
      + " \"" + nodePath + "\"";
    String[] output = this.getRegProcResult(command);
    
    if (output != null) {
      for (int i = 3; i < output.length; i++) {
        if (typeFilter == null) {
          if (output[i].startsWith("HKEY")) {
            result.put(output[i].trim(), null);
          } else if (output[i].startsWith("    ") 
              && output[i].contains("REG_")) {
            String[] line = output[i].trim().split("\t");
          
            ArrayList<String> keyTypeAndValue = new ArrayList<String>();

            keyTypeAndValue.add(line[1].trim());
            keyTypeAndValue.add(line[2].trim());
            result.put(line[0].trim(), keyTypeAndValue);
          }
        } else if (typeFilter.equals(WinRegAccess.NODE)) {
          if (output[i].startsWith("HKEY")) {
            result.put(output[i].trim(), null);
          }
        } else if (output[i].startsWith("    ") && output[i].contains("REG_")) {
          String[] line = output[i].trim().split("\t");
          
          if (line[1].trim().equals(typeFilter)) {
            ArrayList<String> keyTypeAndValue = new ArrayList<String>();

            keyTypeAndValue.add(line[1].trim());
            keyTypeAndValue.add(line[2].trim());
            result.put(line[0].trim(), keyTypeAndValue);
          }
        }
      }
    }
    
    return result;
  }
  
  public ArrayList<String> getNodeKey(String nodePath, String keyName) {
    ArrayList<String> result = new ArrayList<String>();
    String command = WinRegAccess.REG_PROC + " " + WinRegAccess.REG_QUERY 
      + " \"" + nodePath + "\" /v \"" + keyName + "\"";
    String[] output = this.getRegProcResult(command);
    
    if (output != null) {
      for (int i = 3; i < output.length; i++) {
        if (output[i].startsWith("    ") && output[i].contains("REG_")) {
          String[] line = output[i].trim().split("\t");
          
          result.add(line[1].trim());
          result.add(line[2].trim());
        }
      }
    }
    
    return result;
  }
}
