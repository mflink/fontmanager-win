/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fontmanager;
import java.io.InputStream;
import java.io.StringWriter;

/**
 *
 * @author M.Link
 */
public class ProcessOutputReader extends Thread {
  protected Process process;
  protected String output;
  
  public ProcessOutputReader(Process process) {
    this.process = process;
  }
  
  @Override
  public void run() {
    StringWriter stringWriter = new StringWriter();
    InputStream inputStream = this.process.getInputStream();
    
    try {
      int character;
      
      while ((character = inputStream.read()) != -1) {
        stringWriter.write(character);
      }
    
      this.output = stringWriter.toString();
    } catch (Exception e) {
      // Do nothing!
    }
  }     
    
  public String[] getOutput() {
    return this.output.split("\n");
  }
}
