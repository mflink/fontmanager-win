/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fontmanager;
import java.awt.Font;
import java.io.File;

/**
 *
 * @author M.Link
 */
public class FontFile implements Comparable {
  protected File file;
  protected Font font;
  
  public FontFile(File file) {
    this.file = file;
  }
  
  public void load() throws Exception {
    int fontType = 0;
    
    if (this.file.getName().toLowerCase().endsWith(".ttf")) {
      fontType = Font.TRUETYPE_FONT;
    } else {
      throw new Exception(this.file.toString() + ": File not supported");
    }
    
    try {
      this.font = Font.createFont(fontType, this.file);
    } catch (Exception e) {
      throw new Exception(this.file.toString() + ": " + e.getLocalizedMessage());
    }
  }
  
  public Font getFont() {
    return this.font;
  }
  
  public File getFile() {
    return this.file;
  }
  
  @Override
  public String toString() {
    return this.font.getName();
  }
  
  @Override
  public int hashCode() {
    return this.font.toString().hashCode();
  }
  
  @Override
  public boolean equals(Object o) {
    boolean result = false;
    
    if (o instanceof FontFile) {
      if (this.hashCode() == ((FontFile) o).hashCode()) {
        result = true;
      }
    }
    
    return result;
  }

  public int compareTo(Object o) {
    return this.toString().compareTo(o.toString());
  }
}
